package org.mik.fish;

import org.mik.birds.Birds;

public interface Whiteshark extends Birds, Fish {

  public Integer getSizeOfBody();

}