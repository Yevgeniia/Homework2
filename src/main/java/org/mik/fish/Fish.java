package org.mik.fish;

import org.mik.zoo.animal.Animal;

public interface Fish extends Animal {

  public Integer getDeep();

}