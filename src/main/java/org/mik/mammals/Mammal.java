package org.mik.mammals;


import org.mik.zoo.animal.Animal;

public interface Mammal extends Animal {

  public Integer getLengthOfHair();

}