package org.mik.mammals;


public interface Elephant extends Mammal {

  public Integer getLongOfTrunk();

}