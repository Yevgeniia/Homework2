package org.mik.mammals;

public interface Hippopotamus extends Mammal {

  public void getLargeSize();

}