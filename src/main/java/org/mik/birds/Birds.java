package org.mik.birds;

import org.mik.zoo.animal.Animal;

public interface Birds extends Animal {

  public Integer getWingLength();

}