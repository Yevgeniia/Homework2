package org.mik.zoo.animal;

import org.mik.zoopark.AnimalType;
import org.mik.zoopark.LivingBeing;
public interface Animal extends LivingBeing {

    /**
   * 
   * @element-type Animal Type
   */

  public Integer getNumberOfLegs();

  public Integer getNUmberOfTeeth();

  public AnimalType getAnimalType();

  public Integer getAverageWight();

  public Boolean isBird();

  public Boolean isFish();

  public Boolean isMammal();

  public Boolean isAfricanBuffalo();

  public Boolean isElephant();

  public Boolean isHippopotamus();

  public Boolean isRhinoceros();

  public Boolean isOstrich();

  public Boolean isWhiteShark();

}