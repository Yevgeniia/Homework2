package org.mik.zoopark;

public interface LivingBeing {

  public String getScientificName();

  public String getInstanceName();

  public String getImageURL();

}