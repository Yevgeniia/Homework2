package org.mik.zoopark;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractLivingBeing implements LivingBeing {

	private static List<LivingBeing> allLivingBeing = new ArrayList<>();
	private String instanceName;

	private String scientificName;

	private String imageURL;

	public AbstractLivingBeing(String instanceName, String scientificName, String imageURL) {
		super();
		this.instanceName = instanceName;
		this.scientificName = scientificName;
		this.imageURL = imageURL;
		allLivingBeing.add(this);
	}

	@Override
	public String getInstanceName() {
		return getInstanceName();
	}

	/**
	 * @param instanceName
	 *            the instanceName to set
	 */
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	/**
	 * @return the scientificName
	 */
	@Override
	public String getScientificName() {
		return getScientificName();
	}

	/**
	 * @param scientificName
	 *            the scientificName to set
	 */
	public void setScientificName(String scientificName) {
		this.scientificName = scientificName;
	}

	/**
	 * @return the imageURL
	 */
	@Override
	public String getImageURL() {
		return getImageURL();
	}

	/**
	 * @param imageURL
	 *            the imageURL to set
	 */
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	
}